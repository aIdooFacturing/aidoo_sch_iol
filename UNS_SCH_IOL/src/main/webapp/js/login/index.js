var width = window.innerWidth;
var height = window.innerHeight;
var $login_joinBtn,$join_joinBtn,$join_resetBtn;
var $loginEmail, $loginPwd, $joinEmail, $joinPwd, $joinConfirmPwd, $company;

$(function(){
	var video = document.getElementById("bgvid");
	video.play();
	video.loop = true;
	
	setEl();
	chkAutoLogin();
	BindEvt();
});

function chkAutoLogin(){
	var email = window.localStorage.getItem("email");
	var pwd = window.localStorage.getItem("pwd");
	if(email!=null && email !=""){
		$loginEmail.val(email);
		$loginPwd.val(pwd);
		login();
	};
};

var NCArray = new Array();
function getNCList(){
	var url = ctxPath + "/getNCList.do";
	
	$.ajax({
		url : url,
		dataType : "text",
		type : "post",
		success : function(data){
			var json = $.parseJSON(data).NCList;
			
			NCArray = new Array();
			var NC = "";
			var selector = "";
			var endSelect = "";
			
			$(json).each(function(i, data){
				var id = data.id;
				var type = data.type;
				var model = data.model;
				var manufacture = data.manufacture;
						
				if(NC!=manufacture){
					selector += endSelect + "<select id='" + manufacture + "' multiple='multiple' data-native-menu= false data-mini=true>" + 
					"<option value='none' selected=''>" + manufacture + "</option>" + 
					"<option value=" + id + ">" + model + "</option>";
					
					NCArray.push(manufacture);
					endSelect = "</select>";
				}else{
					selector += "<option value=" + id + ">" + model + "</option>";
				};
				
				NC=manufacture;
			});
			
			selector += "</select>";
			$("#NCList").html(selector);
			
			for(var i = 0; i < NCArray.length; i++){
				$("#" + NCArray[i]).selectmenu();
			};
			
			$("#joinForm").css({
				"left" : width/2 - $("#joinForm").width()/2,
				"top" : height/2 - $("#joinForm").height()/2
			});
		
			$(".errMsg").html("");
			$("#loginForm").css("display", "none");
			$("#joinForm").css("display", "inline");
			$joinEmail.focus();
		}
	});
};

function getSelectedItem(){
	var str = "";
	for(var i = 0; i < NCArray.length; i++){
		var item = $("#" + NCArray[i]).val();
		if(item!="none"){
			str += item + ",";
		};
	};
	return replaceAll(str,"none,","");
};

function BindEvt(){
	$("#login input[id=email], #login input[id=pwd], #login input[id=saveId]").bind("keyup", function(e){
		if(e.keyCode==13){
			login();
		};
	});
	
	$("#join input[id=email], #join input[id=pwd], #join input[id=confirmPwd]").bind("keyup", function(e){
		if($joinEmail.val() != "" && $joinPwd.val() != "" && $joinConfirmPwd.val() != ""){
			$join_joinBtn.attr("disabled", false);
		}else{
			$join_joinBtn.attr("disabled", true);
		};
	});
	
	$login_joinBtn.click(join);
	//$login_loginBtn.click(login);			      
	$join_joinBtn.click(joinCompl);
	$join_resetBtn.click(formReset);
	$("#backArrow").click(goLoginForm);
};

function goLoginForm(){
	$("#loginForm").css("display", "inline");
	$("#joinForm").css("display", "none");
	$(".errMsg").html("");
	$("#login input[id=email], #login input[id=pwd]").val("");
};

function formReset(){
	$("#join input[id=email], #join input[id=pwd], #join input[id=confirmPwd], #join #company").val("");
	$join_joinBtn.attr("disabled", true);
	$(".errMsg").html("");
	
	for(var i = 0; i <NCArray.length; i++){
		$("#" + NCArray[i]).val("none").change();
	};
	$joinEmail.focus();
	return false;
};

function join(){
	formReset();
	getNCList();
	return false;
};

function replaceAll(str, source, target){
	return str.split(source).join(target);
};

function joinCompl(){
	var NCList = getSelectedItem();
	var email = $joinEmail.val();
	var pwd = $joinPwd.val();
	var company = $company.val();
	var confirmPwd = $joinConfirmPwd.val();

	if(pwd!=confirmPwd){
		$(".errMsg").html("Passwords did not match. Please try again");
		var pwd = $joinPwd.focus();
		return;
	};
	
	var url = ctxPath + "/join.do";
	var param = "email=" + email + 
				"&pwd=" + pwd +
				"&company=" + company + 
				"&NC=" + NCList;
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type : "post",
		success :function(data){
			if(data=="success"){
				goLoginForm();
				$loginEmail.focus();
			}else if(data=="dupl"){
				$(".errMsg").html("E-mail Address Already in Use");
				$joinEmail.focus();
			};
		}
	});
	return false;
};

function setEl(){
	$login_joinBtn = $("#login font[id=joinBtn]");
	$join_joinBtn = $("#join button[id=joinBtn]");
	$join_resetBtn = $("#join button[id=resetBtn]");
	$loginEmail = $("#login input[id=email]");
	$loginPwd = $("#login input[id=pwd]"); 
	$joinEmail = $("#join input[id=email]"); 
	$joinPwd = $("#join input[id=pwd]"); 
	$joinConfirmPwd = $("#join input[id=confirmPwd]");
	$company = $("#join #company");
	$loginEmail.focus();
	
	$("#corver").css({
		"width" : width,
		"height" : height
	});
	
	$("#loginForm").css({
		"width" : 350,
		"height" : 300
	});
	
	$("#joinForm").css({
		"width" : 350,
	});
	
	$("#loginForm").css({
		"left" : width/2 - $("#loginForm").width()/2,
		"top" : height/2 - $("#loginForm").height()/2
	});

	$join_joinBtn.attr("disabled", true);
};

function login(){
	var url = ctxPath + "/login.do";
	var email = $loginEmail.val();
	var pwd = $loginPwd.val();
	var param = "email=" + email + 
				"&pwd=" + pwd;
	
	if($("#saveId").is(":checked")){
		window.localStorage.setItem("email", email);
		window.localStorage.setItem("pwd", pwd);
	};
	
	$.ajax({
		url : url,
		data : param,
		dataType : "text",
		type :"post",
		success : function(data){
			if(data=="fail"){
				$(".errMsg").html("Your E-mail or password was entered incorrectly.");
			}else{
				//window.location=ctxPath + "/chart/main.do";
				window.sessionStorage.setItem("company", data);
				if(email=="demo"){
					window.location=ctxPath + "/chart/chart1.do";
				}else{
					window.location=ctxPath + "/chart/chart2.do";
				};
			};
		}
	});
};