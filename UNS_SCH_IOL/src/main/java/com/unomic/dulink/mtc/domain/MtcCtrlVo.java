package com.unomic.dulink.mtc.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import com.unomic.dulink.common.domain.PagingVo;

@Getter
@Setter
@ToString
public class MtcCtrlVo{
	Integer dvcId;
	Integer isChg;
	String chgTy;
	String chgDt;
	String regdt;
}
