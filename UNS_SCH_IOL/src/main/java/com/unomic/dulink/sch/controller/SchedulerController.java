package com.unomic.dulink.sch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.sch.domain.IolVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;
	
	private final Boolean SET_OFF_SCH = false;
//	private final Boolean SET_OFF_SCH = true;
	
	@Scheduled(fixedDelay = 10000)
	public void schIol2(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "2"; //key
		String dvcIp = "10.33.74.232"; // CommonCode.MAP_MSG_IOL_IP.get(key)
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	

	@Scheduled(fixedDelay = 10000)
	public void schIol3(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "3";
		String dvcIp = "10.33.74.225";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol4(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "4";
		String dvcIp = "10.33.74.224";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol5(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "5";
		String dvcIp = "10.33.74.223";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol9(){
		if(SET_OFF_SCH) return;

		String dvcId = "9";
		String dvcIp = "10.33.74.236";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol10(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "10";
		String dvcIp = "10.33.74.237";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol18(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "18";
		String dvcIp = "10.33.74.233";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol19(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "19";
		String dvcIp = "10.33.74.234";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol28(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "28";
		String dvcIp = "10.33.74.222";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol43(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "43";
		String dvcIp = "10.33.74.212";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol31(){
		if(SET_OFF_SCH) return;

		String dvcId = "31";
		String dvcIp = "10.33.74.211";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}

	@Scheduled(fixedDelay = 10000)
	public void schIol32(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "32";
		String dvcIp = "10.33.74.220";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}

	@Scheduled(fixedDelay = 10000)
	public void schIol33(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "33";
		String dvcIp = "10.33.74.221";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol34(){
		if(SET_OFF_SCH) return;
		
		String dvcId = "34";
		String dvcIp = "10.33.74.216";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol35(){
		if(SET_OFF_SCH) return;

		String dvcId = "35";
		String dvcIp = "10.33.74.217";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol40(){
		if(SET_OFF_SCH) return;

		String dvcId = "40";
		String dvcIp = "10.33.74.218";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		//LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000) 
	public void schIol29(){
		if(SET_OFF_SCH) return;

		String dvcId = "29";
		String dvcIp = "10.33.74.230";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
//		LOGGER.info("rtnVo:"+rtnVo);
	}
	
	@Scheduled(fixedDelay = 10000)
	public void schIol70(){
		if(SET_OFF_SCH) return;

		String dvcId = "70";
		String dvcIp = "10.33.56.229";
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
//		LOGGER.info("URL:"+URL);
	}
	
}